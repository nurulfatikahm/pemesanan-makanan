package com.example.cafe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button openPageTambah = findViewById(R.id.btnTambah);
        openPageTambah.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent openPageIntent = new Intent(MainActivity.this, Pesanan.class);
                startActivity(openPageIntent);
            }

        });

        Button openPageSemua = findViewById(R.id.btnList);
        openPageSemua.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent openPageIntent = new Intent(MainActivity.this, MenuCafe.class);
                startActivity(openPageIntent);
            }

        });
    }
}
