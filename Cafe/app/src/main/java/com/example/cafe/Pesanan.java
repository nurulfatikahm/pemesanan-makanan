package com.example.cafe;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Pesanan extends AppCompatActivity {

    Button btnTambah;
    EditText editNoMeja, editKodeMenu, editHarga, editTanggal, editJam;
    TextView txtNoMeja, txtKodeMenu, txtHarga, txtNama, txtTanggal, txtJam;
    private Spinner sp_nama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesanan);

        Button btnTambah = (Button) findViewById(R.id.btnTambah);
        editNoMeja = (EditText) findViewById(R.id.editNoMeja);
        editHarga = (EditText) findViewById(R.id.editHarga);
        editTanggal = (EditText) findViewById(R.id.editTanggal);
        editJam = (EditText) findViewById(R.id.editJam);

        txtTanggal = (TextView) findViewById(R.id.txtTanggal);
        txtJam = (TextView) findViewById(R.id.txtJam);
        txtNoMeja = (TextView) findViewById(R.id.txtNoMeja);
        txtHarga = (TextView) findViewById(R.id.txtHarga);
        txtNama = (TextView) findViewById(R.id.txtNama);
        final Spinner txtSpinner = (Spinner) findViewById(R.id.spinner);

        txtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            String spineervalue;

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                spineervalue = txtSpinner.getSelectedItem().toString();
                if (spineervalue.equals("B01")) {
                    txtNama.setText("Kopi Hitam");
                } else if (spineervalue.equals("B02")) {
                    txtNama.setText("Cappucino");
                } else if (spineervalue.equals("B03")) {
                    txtNama.setText("Sparkling Tea");
                } else if (spineervalue.equals("M01")) {
                    txtNama.setText("Batagor");
                } else if (spineervalue.equals("M02")) {
                    txtNama.setText("Cireng");
                } else if (spineervalue.equals("M03")) {
                    txtNama.setText("Nasi Goreng");
                } else if (spineervalue.equals("D01")) {
                    txtNama.setText("Cheese Cake");
                } else if (spineervalue.equals("D02")) {
                    txtNama.setText("Black Salad");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        //tampilkan pesanan
        btnTambah.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                final String noMeja = editNoMeja.getText().toString();
                final String harga = editHarga.getText().toString();
                final String tanggal = editTanggal.getText().toString();
                final String jam = editJam.getText().toString();

                txtNoMeja.setText(noMeja);
                txtHarga.setText(harga);
                txtJam.setText(jam);
                txtTanggal.setText(tanggal);


            }
        });


    }
}
